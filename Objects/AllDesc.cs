﻿using MaterialIncomeCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaterialIncomeCore.Objects
{
    public class AllDesc
    {
        public Supplier[] Suppliers { get; set; }
        public Carrier[] Carriers { get; set; }
        public Note[] Notes { get; set; }
        public LicencePlates[] LicencePlates { get; set; }
        public CarSize[] CarSizes { get; set; }
        public Statuses[] Statuses { get; set; }
        public string[] Accepted { get; internal set; }
    }
}
