import { Checkbox, createStyles, makeStyles, TableCell, TableHead, TableRow, TableSortLabel, TextField, Theme } from "@material-ui/core";
import React, { ChangeEvent } from "react";
import { GetIncomeColumnsMaterial, HeadCell, IncomeObject } from "../../objects/IncomeObject";
import { Order } from "../../screens/ListScreen";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        visuallyHidden: {
            border: 0,
            clip: 'rect(0 0 0 0)',
            height: 1,
            margin: -1,
            overflow: 'hidden',
            padding: 0,
            position: 'absolute',
            top: 20,
            width: 1,
        },
    }),
);


const headCells: HeadCell[] = GetIncomeColumnsMaterial()

interface EnhancedTableProps {
    numSelected: number;
    onRequestSort: (event: React.MouseEvent<unknown>, property: keyof IncomeObject) => void;
    onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
    order: Order;
    orderBy: string;
    rowCount: number;
    filterChanged: (id: string, value: string) => void;
}

function EnhancedTableHead(props: EnhancedTableProps) {
    const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort, filterChanged } = props;
    const createSortHandler = (property: keyof IncomeObject) => (event: React.MouseEvent<unknown>) => {
        onRequestSort(event, property);
    };
    const classes = useStyles();

    return (
        <TableHead>
            <TableRow>
                <TableCell padding="checkbox">
                    <Checkbox
                        indeterminate={numSelected > 0 && numSelected < rowCount}
                        checked={rowCount > 0 && numSelected === rowCount}
                        onChange={onSelectAllClick}
                        inputProps={{ 'aria-label': 'select all desserts' }}
                    />
                </TableCell>
                {headCells.map((headCell) => (
                    <TableCell
                        key={headCell.id}
                        align={'center'}
                        padding={headCell.disablePadding ? 'none' : 'default'}
                        sortDirection={orderBy === headCell.id ? order : false}
                    >
                        <TableSortLabel
                            active={orderBy === headCell.id}
                            direction={orderBy === headCell.id ? order : 'asc'}
                            onClick={createSortHandler(headCell.id)}
                        >
                            {headCell.label}

                            {orderBy === headCell.id ? (
                                <span className={classes.visuallyHidden}>
                                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                </span>
                            ) : null}
                        </TableSortLabel>
                        <TextField id={headCell.id} onChange={(event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => filterChanged(headCell.id, event.target.value)} />
                    </TableCell>
                ))}

                <TableCell>Upravit</TableCell>
                <TableCell>Smazat</TableCell>
            </TableRow>
        </TableHead>
    );
}

export default EnhancedTableHead;