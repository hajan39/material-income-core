import React from 'react';
import { useTheme } from '@material-ui/core/styles';
import { LineChart, Line, XAxis, YAxis, Label, ResponsiveContainer } from 'recharts';
import Title from './Title';
import { IncomeObject } from '../objects/IncomeObject';
import moment from 'moment';


export interface ChartProps {
  data: IncomeObject[]
}

export default function Chart(props: ChartProps) {
  const theme = useTheme();
  var data = props.data.sort((x, y) => moment(x.arriveTime).diff(moment(y.arriveTime))).map((x, index) => { return { time: moment(x.arriveTime).format("HH:mm"), count: index + 1 } })
  return (
    <React.Fragment>
      <Title>Dnes</Title>
      <ResponsiveContainer>
        <LineChart
          data={data}

          margin={{
            top: 16,
            right: 16,
            bottom: 0,
            left: 24,
          }}
        >
          <XAxis dataKey="time" stroke={theme.palette.text.secondary} domain={["00:00", '23:59']} />
          <YAxis stroke={theme.palette.text.secondary} label={{ value: 'Pocet dodavek', angle: -90, position: 'center' }}>
            {/* <Label
          
             // rotate={270}
              position="left"
              style={{ textAnchor: 'middle', fill: theme.palette.text.primary }}
            >
              Pocet dodavek
            </Label> */}
          </YAxis>
          <Line type="monotone" dataKey="count" stroke={theme.palette.primary.main} dot={true} />
        </LineChart>
      </ResponsiveContainer>
    </React.Fragment>
  );
}
