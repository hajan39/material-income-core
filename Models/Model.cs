﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace MaterialIncomeCore.Models
{
    public class MaterialContext : DbContext
    {

        public MaterialContext(DbContextOptions<MaterialContext> options)
           : base(options)
        {
        }
        public DbSet<Container> CONTAINER_DESC { get; set; }
        public DbSet<SupplierAddress> SUPPLIER_ADDRESS { get; set; }
        public DbSet<SupplierPrice> SUPPLIER_PRICE { get; set; }
        public DbSet<Note> NOTE { get; set; }
        public DbSet<DisponentNote> DISPONENT_NOTE { get; set; }
        public DbSet<Carrier> CARRIER_DESC { get; set; }
        public DbSet<Delivery> DELIVERY { get; set; }
        public DbSet<Supplier> SUPPLIER_DESC { get; set; }
        public DbSet<LicencePlates> LICENCE_PLATE_DESC { get; set; }
        public DbSet<Statuses> STATUSES_DESC { get; set; }
        public DbSet<CarSize> CAR_SIZE_DESC { get; set; }

        #region Required
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Container>().HasKey(x => x.Id);
            modelBuilder.Entity<SupplierAddress>().HasKey(x => x.Id);
            modelBuilder.Entity<SupplierPrice>().HasKey(x => x.Id);
            modelBuilder.Entity<Note>().HasKey(x => x.Id);
            modelBuilder.Entity<DisponentNote>().HasKey(x => x.Id);
            modelBuilder.Entity<Carrier>().HasKey(x => x.Id);
            modelBuilder.Entity<Delivery>().HasKey(x => x.Id);
            modelBuilder.Entity<Supplier>().HasKey(x => x.Id);
            modelBuilder.Entity<LicencePlates>().HasKey(x => x.Id);
            modelBuilder.Entity<Statuses>().HasKey(x => x.Id);
            modelBuilder.Entity<CarSize>().HasKey(x => x.Id);
        }
        #endregion

    }
    public class Container
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }

        [Column("CONTAINER_NAME")]
        public string ContainerName { get; set; }
    }

    public class SupplierAddress
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("NAME")]
        public string Name { get; set; }
        [Column("ADDRESS")]
        public string Address { get; set; }
        [Column("CITY")]
        public string City { get; set; }

    }

    public class SupplierPrice
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("NAME")]
        public string Name { get; set; }

        [Column("CAR_SIZE")]
        public string CarSize { get; set; }

        [Column("IMPORT_SK")]
        public decimal? ImportSk { get; set; }

        [Column("EXPORT_SK")]
        public decimal? ExportSk { get; set; }
    }

    public class Note
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("NOTE")]
        public string Value { get; set; }
    }

    public class DisponentNote
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("DATE")]
        public DateTime? Date { get; set; }

        [Column("ARRIVE_TIME")]
        public DateTime? ArriveTime { get; set; }

        [Column("DEPARTURE_TIME")]
        public DateTime? DepartureTime { get; set; }
        [Column("SUPPLIER")]
        public string Supplier { get; set; }
        [Column("CARRIER")]
        public string Carrier { get; set; }

        [Column("LICENCE_PLATE")]
        public string LicencePlate { get; set; }

        [Column("CAR_SIZE")]
        public string CarSize { get; set; }
        [Column("WEIGHT")]
        public int? Weight { get; set; }

        [Column("PALLET_COUNT")]
        public int? PalletCount { get; set; }

        [Column("PALLET_TYPE")]
        public string PalletType { get; set; }

        [Column("IMPORT_PRICE")]
        public int? ImportPrice { get; set; }
        [Column("INVOICE")]
        public string Invoice { get; set; }

        [Column("DELIVERY_LIST")]
        public string DeliveryList { get; set; }
        [Column("CMR")]
        public string Cmr { get; set; }
        [Column("GRN")]
        public string Grn { get; set; }

        [Column("DISPATCHER_NOTE")]
        public string DispatcherNote { get; set; }

        [Column("ACCEPTED_NAME")]
        public string AcceptedName { get; set; }
        [Column("CCS")]
        public string Ccs { get; set; }
    }


    public class Carrier
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }

        [Column("CARRIER_NAME")]
        public string CarrierName { get; set; }
    }

    public class Delivery
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        public DateTime? DATE { get; set; }

        [Column("ARRIVE_TIME")]
        public DateTime? ArriveTime { get; set; }

        [Column("DEPARTURE_TIME")]
        public DateTime? DepartureTime { get; set; }
        [Column("SUPPLIER")]
        public string Supplier { get; set; }
        [Column("CARRIER")]
        public string Carrier { get; set; }

        [Column("LICENCE_PLATE")]
        public string LicencePlate { get; set; }
        [Column("CAR_SIZE")]
        public string CarSize { get; set; }
        [Column("WEIGHT")]
        public int? Weight { get; set; }

        [Column("GRN")]
        public string Grn { get; set; }
        [Column("PALLET_COUNT")]
        public string PalletCount { get; set; }

        [Column("PALLET_DESC")]
        public string PalletDesc { get; set; }

        [Column("REGRESSION_ID")]
        public string RegressionId { get; set; }

        [Column("IMPORT_PRICE")]
        public int? ImportPrice { get; set; }
        [Column("INVOICE")]
        public string Invoice { get; set; }

        [Column("DELIVERY_LIST")]
        public string DeliveryList { get; set; }
        [Column("CMR")]
        public string Cmr { get; set; }

        [Column("RECIEPT_NOTE")]
        public string RecieptNote { get; set; }

        [Column("DISPATCHERS_NOTE")]
        public string DispatchersNote { get; set; }
        [Column("ACCEPTED")]
        public string Accepted { get; set; }
        [Column("CCS")]
        public string Ccs { get; set; }
        [Column("STATUS")]
        public string Status { get; set; }
        [Column("DELETED")]
        [JsonIgnore]
        public DateTime? Deleted { get; set; }
    }

    public class Supplier
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }

        [Column("SUPPLIER_NAME")]
        public string SupplierName { get; set; }
    }
    public class LicencePlates
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }

        [Column("LICENCE_PLATE")]
        public string LicencePlate { get; set; }
    }

    public class Statuses
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("STATUSES")]
        public string Status { get; set; }
    }
    public class CarSize
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }

        [Column("CAR_SIZE")]
        public string Size { get; set; }
    }
}
