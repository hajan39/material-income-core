import axios from "axios";
import { Carrier, CarSize, AllDesc, DisponentNote, IncomeObject, LicencePlate, MaterialContainer, Note, Statuses, Supplier, SupplierAddress, SupplierPrice, IncomeObjectRequest } from "../objects/IncomeObject";

const url: string = 'materialincome';
export const getData = async (): Promise<IncomeObject[]> => {
    return axios.get<IncomeObject[]>(url).then(x => x.data);

}

export const createRecord = async (data: IncomeObjectRequest): Promise<IncomeObject[]> => {
    await axios.post<IncomeObject>(url, data);
    return axios.get<IncomeObject[]>(url).then(x => x.data);
}
export const updateRecord = async (data: IncomeObjectRequest): Promise<IncomeObject[]> => {
    await axios.put<IncomeObject>(url + "/" + data.id, data);
    return axios.get<IncomeObject[]>(url).then(x => x.data);
}
export const deleteRecord = async (id: number | string): Promise<IncomeObject[]> => {
    await axios.delete(url + "/" + id);
    return axios.get<IncomeObject[]>(url).then(x => x.data);
}

export const GetLicences = async (): Promise<LicencePlate[]> => {
    return axios.get<LicencePlate[]>(url + "/lic").then(x => x.data);
}

export const GetContainer = async (): Promise<MaterialContainer[]> => {

    return axios.get<MaterialContainer[]>(url + "/Container").then(x => x.data);

}

export const GetSupplierAddress = async (): Promise<SupplierAddress[]> => {
    return axios.get<SupplierAddress[]>(url + "/supplierAddress").then(x => x.data);
}
export const GetSupplierPrice = async (): Promise<SupplierPrice[]> => {

    return axios.get<SupplierPrice[]>(url + "/supplierPrice").then(x => x.data);

}
export const GetNote = async (): Promise<Note[]> => {

    return axios.get<Note[]>(url + "/Note").then(x => x.data);

}
export const GetDisponentNote = async (): Promise<DisponentNote[]> => {

    return axios.get<DisponentNote[]>(url + "/DisponentNote").then(x => x.data);

}
export const GetCarrier = async (): Promise<Carrier[]> => {

    return axios.get<Carrier[]>(url + "/Carrier").then(x => x.data);

}
export const GetSupplier = async (): Promise<Supplier[]> => {

    return axios.get<Supplier[]>(url + "/Supplier").then(x => x.data);

}
export const GetStatuses = async (): Promise<Statuses[]> => {

    return axios.get<Statuses[]>(url + "/Statuses").then(x => x.data);

}
export const GetCarSize = async (): Promise<CarSize[]> => {

    return axios.get<CarSize[]>(url + "/CarSize").then(x => x.data);

}

export const GetAllDesc = async (): Promise<AllDesc> => {
    return axios.get<AllDesc>(url + "/allDesc").then(x => x.data);

}
export const GetSpecificIncome = async (id: string): Promise<IncomeObject> => {
    return axios.get<IncomeObject>(url + "/delivery/" + id).then(x => x.data);

}

