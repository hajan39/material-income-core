import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  withRouter,
  Link,
  NavLink,
} from "react-router-dom";
import HomeScreen from './screens/HomeScreen';
import ListScreen from './screens/ListScreen';
import { RouterProps } from 'react-router';
import InsertScreen from './screens/InsertScreen';
import { Button, ButtonGroup } from '@material-ui/core';
import { IncomeObject } from './objects/IncomeObject';
import { getData } from './workers/backendWorker';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center" style={{ display: "sticky", bottom: 0 }}>
      {'Copyright © '}
      <Link to="https://hajan.com/">
        Hajan Developer
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    justifyContent: "space-around",
    alignContent: "center"
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    // ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    marginBottom: 20
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(5),
  }
}));

const Dashboard: React.FunctionComponent<RouterProps> = ({ history }) => {
  const classes = useStyles();

  const [data, setData] = useState<IncomeObject[]>([]);
  useEffect(() => {
    getData().then(x => {
      setData(x);
    });
  }, [])

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="absolute" className={classes.appBar}>
        <Toolbar className={classes.toolbar}>
          <Button onClick={() => history.push("/")}>
            <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
              Prehled
          </Typography>
          </Button>
          <Button onClick={() => history.push("/insert")}>
            <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
              Zadavani
          </Typography>
          </Button>
          <Button onClick={() => history.push("/list")}>
            <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
              Prohlizeni
          </Typography>
          </Button>

        </Toolbar>
      </AppBar>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} style={{ padding: 30 }} />

        <Switch>
          <Route path="/list">
            <ListScreen rowsInput={data} updateData={(data: IncomeObject[]) => setData(data)} />
          </Route>

          <Route exact path="/insert" >
            <InsertScreen updateData={(data: IncomeObject[]) => setData(data)} />
          </Route>
          <Route path="/insert/:id" >
            <InsertScreen updateData={(data: IncomeObject[]) => setData(data)} />
          </Route>
          <Route exact path="/">
            <HomeScreen data={data} />
          </Route>
        </Switch>

        <Box pt={4}>
          <Copyright />
        </Box>
      </main>
    </div>
  );
}

export default withRouter(Dashboard);