import React, { ChangeEvent, RefObject, useEffect, useRef, useState } from 'react'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { Button, Collapse, Container, IconButton, TextField } from '@material-ui/core';
import { AllDesc, Carrier, CarSize, GetDefaultIncomeObjectRequest, IncomeObject, IncomeObjectRequest, LicencePlate, Note, Statuses, Supplier } from '../objects/IncomeObject';
import { createRecord, GetAllDesc, GetSpecificIncome, updateRecord } from '../workers/backendWorker';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { useParams } from "react-router-dom";
import CircularProgress from '@material-ui/core/CircularProgress';
import CloseIcon from '@material-ui/icons/Close';
import moment from 'moment';
import { Alert } from '@material-ui/lab';
export interface InsertScreenProps {
    updateData: (data: IncomeObject[]) => void
}
interface InsertScreenParamTypes {
    id?: string
}
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        paper: {
            textAlign: 'center',
            color: theme.palette.text.secondary,
        },
        textField: {
        },
        formControl: {
            minWidth: 120,
        },
        selectEmpty: {
        },
    }),
);
const useForm = (initialState: IncomeObjectRequest = GetDefaultIncomeObjectRequest(), onSubmit: (formData: IncomeObjectRequest) => void) => {
    const [formData, setFormData] = React.useState<IncomeObjectRequest>(initialState);

    const handleInputChange = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        console.log("setting change", e.target.type, e.target.id, e.target.value);

        switch (e.target.type) {
            case "number":
                setFormData({ ...formData, [e.target.id]: parseFloat(e.target.value) || e.target.value })
                break;
            case "time":
                setFormData({ ...formData, [e.target.id]: moment(e.target.value, "HH:mm").format("YYYY-MM-DDTHH:mm:ss") })

                break;
            case "date":
            case "text":
            default:
                setFormData({ ...formData, [e.target.id]: e.target.value })

                break;
        }
    }

    const handleValueChange = (id: keyof IncomeObjectRequest, value: string | null) => {
        setFormData({ ...formData, [id]: value })
    }
    const handleFormDataChange = (newFormData: IncomeObject | IncomeObjectRequest) => {
        setFormData(newFormData)
    }

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        onSubmit?.(formData);
    }

    return { formData, handleInputChange, handleValueChange, handleSubmit, handleFormDataChange };
}
const InsertScreen: React.FunctionComponent<InsertScreenProps> = ({ updateData }) => {
    const myFormRef = useRef<HTMLFormElement>(null);
    const [confirmText, setConfirmText] = useState<string>()
    const [error, setError] = React.useState<string>();
    const [allDesc, setAllDesc] = useState<AllDesc>();
    const { formData, handleInputChange, handleValueChange, handleSubmit, handleFormDataChange } = useForm(
        GetDefaultIncomeObjectRequest(),
        (formData: IncomeObjectRequest) => {
            console.table(formData);
            if (params.id) {
                updateRecord(formData).then((x: IncomeObject[]) => {
                    updateData(x);
                    var refreshitem = x.find(t => t.id.toString() == params.id?.toString());
                    refreshitem && handleFormDataChange(refreshitem);
                    setConfirmText("Aktualizace byla uspesna!")
                }).catch(x => {
                    setError(x.toString())
                })
            } else {
                createRecord(formData).then((x: IncomeObject[]) => {
                    updateData(x);
                    resetForm();
                    setConfirmText("Vytvoreni zaznamu bylo uspesne!")

                }).catch(x => {
                    setError(x.toString())
                });
            }
        }
    );

    let params = useParams<InsertScreenParamTypes>();
    const [loading, setLoading] = useState(!!params.id)
    const classes = useStyles();
    useEffect(() => {
        GetAllDesc().then(x => setAllDesc(x));
        params.id && GetSpecificIncome(params.id).then(x => {
            handleFormDataChange(x);
            setLoading(false);
        })
    }, [])
    const resetForm = () => {
        if (!!myFormRef.current)
            myFormRef.current.reset();
    }


    if (loading) {
        return <Container style={{ marginTop: 10, textAlign: "center", width: "80vw", height: "80vh" }}>
            <CircularProgress />
        </Container>
    } else
        return (
            <Container style={{ marginTop: 10 }}>
                <Collapse in={!!error || !!confirmText} onEnter={() => setTimeout(() => { setError(undefined); setConfirmText(undefined); }, (!!error ? 15000 : 5000))}>
                    <Alert variant="filled" severity={!!error && "error" || !!confirmText && "success" || "info"}
                        action={
                            <IconButton
                                aria-label="close"
                                color="inherit"
                                size="small"
                                onClick={() => {
                                    setError(undefined);
                                    setConfirmText(undefined);
                                }}
                            >
                                <CloseIcon fontSize="inherit" />
                            </IconButton>
                        }
                    >
                        {error}{confirmText}
                    </Alert>
                </Collapse>
                <form ref={myFormRef} className={classes.root} noValidate autoComplete="off" onSubmit={handleSubmit}>
                    <Grid container spacing={3}>
                        <Grid item xs={3}>
                            <TextField fullWidth
                                id="id"
                                label="Číslo"
                                type="number"
                                value={formData.id}
                                disabled
                                defaultValue={formData.id}
                                onChange={handleInputChange}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={3}>
                            <TextField fullWidth
                                id="date"
                                name="date"
                                label="Dátum"
                                type="date"
                                className={classes.textField}
                                value={moment(formData.date).format("YYYY-MM-DD")}
                                defaultValue={moment().format("YYYY-MM-DD")}
                                onChange={handleInputChange}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField fullWidth id="invoice" label="Faktúra"
                                onChange={handleInputChange}
                                value={formData.invoice}
                                defaultValue={formData.invoice} />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField fullWidth
                                id="arriveTime"
                                label="Čas príjazdu"
                                type="time"
                                onChange={handleInputChange}
                                value={moment(formData.arriveTime).format("HH:mm")}
                                defaultValue={moment().format("HH:mm")}
                                className={classes.textField}
                                InputLabelProps={{
                                    shrink: true,
                                }} />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField fullWidth id="deliveryList" label="Dodací list"
                                onChange={handleInputChange}
                                value={formData.deliveryList}
                                defaultValue={formData.deliveryList} />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField fullWidth
                                id="departureTime"
                                label="Čas odjazdu"
                                type="time"
                                onChange={handleInputChange}
                                value={moment(formData.departureTime).format("HH:mm")}
                                defaultValue={moment().format("HH:mm")}
                                className={classes.textField}
                                InputLabelProps={{
                                    shrink: true,
                                }} />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField fullWidth id="cmr" label="CMR"
                                onChange={handleInputChange}
                                value={formData.cmr}
                                defaultValue={formData.cmr} />
                        </Grid>
                        <Grid item xs={6}>
                            <Autocomplete
                                options={allDesc?.suppliers || []}
                                freeSolo
                                getOptionLabel={(option: Supplier) => option.supplierName}
                                id="supplier"
                                inputValue={formData.supplier ? formData.supplier : ""}
                                // defaultValue={formData.supplier || ""}
                                onInputChange={(event, newInputValue) => {
                                    handleValueChange("supplier", newInputValue);
                                }}
                                renderInput={(params) => <TextField {...params} label="Dodávatel" />}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField fullWidth id="grn" label="GRN"
                                onChange={handleInputChange}
                                value={formData.grn}
                                defaultValue={formData.grn} />
                        </Grid>
                        <Grid item xs={6}>
                            <Autocomplete
                                freeSolo
                                options={allDesc?.carriers || []}
                                getOptionLabel={(option: Carrier) => option.carrierName}
                                id="carrier"
                                inputValue={formData.carrier ? formData.carrier : ""}
                                // defaultValue={formData.carrier || ""}
                                onInputChange={(event, newInputValue) => {
                                    handleValueChange("carrier", newInputValue);
                                }}

                                renderInput={(params) => <TextField {...params} label="Prepravca" />}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <Autocomplete
                                freeSolo
                                options={allDesc?.notes || []}
                                getOptionLabel={(option: Note) => option.value}
                                id="recieptNote"
                                inputValue={formData.recieptNote !== null ? formData.recieptNote : ""}
                                // defaultValue={formData.recieptNote !== null ? formData.recieptNote : ""}
                                onInputChange={(event, newInputValue) => {
                                    handleValueChange("recieptNote", newInputValue);
                                }}
                                renderInput={(params) => <TextField {...params} label="Poznámka" />}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <Autocomplete
                                freeSolo
                                options={allDesc?.licencePlates || []}
                                getOptionLabel={(option: LicencePlate) => option.licencePlate}
                                id="licencePlate"
                                inputValue={formData.licencePlate ? formData.licencePlate : ""}
                                // defaultValue={formData.licencePlate || ""}
                                onInputChange={(event, newInputValue) => {
                                    handleValueChange("licencePlate", newInputValue);
                                }}
                                renderInput={(params) => <TextField {...params} label="ŠPZ" />}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <Autocomplete
                                freeSolo
                                options={allDesc?.accepted || []}
                                getOptionLabel={(option: string) => option}
                                id="accepted"
                                inputValue={formData.accepted ? formData.accepted : ""}
                                // defaultValue={formData.licencePlate || ""}
                                onInputChange={(event, newInputValue) => {
                                    handleValueChange("accepted", newInputValue);
                                }}
                                renderInput={(params) => <TextField {...params} label="Prijal" />}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <Autocomplete
                                freeSolo
                                options={allDesc?.carSizes || []}
                                getOptionLabel={(option: CarSize) => option.size}
                                id="carSize"
                                inputValue={formData.carSize ? formData.carSize : ""}
                                // defaultValue={formData.carSize || ""}
                                onInputChange={(event, newInputValue) => {
                                    handleValueChange("carSize", newInputValue);
                                }}
                                renderInput={(params) => <TextField {...params} label="Velikost auta" />}
                            />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField fullWidth id="ccs" label="CCS"
                                onChange={handleInputChange}
                                value={formData.ccs}
                                defaultValue={formData.ccs} />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField fullWidth
                                id="weight"
                                label="Hmotnosť kg"
                                onChange={handleInputChange}
                                value={formData.weight}
                                defaultValue={formData.weight}
                                type="number" />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField fullWidth
                                id="regressionId"
                                label="Číslo regresu"
                                onChange={handleInputChange}
                                value={formData.regressionId}
                                defaultValue={formData.regressionId}
                                type="text" />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField fullWidth
                                id="palletCount"
                                label="Počet palet jedn"
                                onChange={handleInputChange}
                                value={formData.palletCount}
                                defaultValue={formData.palletCount}
                                type="text" />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField fullWidth
                                id="importPrice"
                                onChange={handleInputChange}
                                value={formData.importPrice}

                                defaultValue={formData.importPrice}
                                label="Cena import"
                                type="number" />
                        </Grid>
                        <Grid item xs={6}>
                            <TextField fullWidth
                                id="palletDesc"
                                onChange={handleInputChange}
                                value={formData.palletDesc}
                                defaultValue={formData.palletDesc}
                                label="Typ palet jedn" />
                        </Grid>
                        <Grid item xs={2}>
                            <Autocomplete
                                freeSolo
                                options={allDesc?.statuses || []}
                                getOptionLabel={(option: Statuses) => option.status}
                                id="status"
                                inputValue={formData.status ? formData.status : ""}
                                // defaultValue={formData.status || ""}
                                onInputChange={(event, newInputValue) => {
                                    handleValueChange("status", newInputValue);
                                }}
                                renderInput={(params) => <TextField {...params} label="Stav Polozky" />}
                            />
                        </Grid>
                        <Grid item xs={3}>
                            <Button style={{ width: "100%" }} variant="contained" size="large" type="submit" color="primary">ULOŽ</Button>
                        </Grid>
                        <Grid item xs={1} style={{ alignSelf: "center", textAlign: "end" }}>
                            <Button variant="outlined" size="small" onClick={() => {
                                resetForm();
                                handleFormDataChange(GetDefaultIncomeObjectRequest())
                            }
                            } type="button" color="secondary">ZMAŽ</Button>
                        </Grid>
                    </Grid>
                </form>
            </Container >
        );
}

export default InsertScreen;