import { Grid, Paper } from '@material-ui/core';
import React from 'react'
import clsx from 'clsx';
import Chart from '../components/Chart';
import Deposits from '../components/Deposits';
import Orders from '../components/Orders';
import { makeStyles } from '@material-ui/core';
import { IncomeObject } from '../objects/IncomeObject';
import moment from 'moment';


export interface HomeScreenProps {
    data: IncomeObject[]
}
const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 240,
    },
}));
const HomeScreen: React.FunctionComponent<HomeScreenProps> = ({ data }) => {

    const classes = useStyles();

    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
    const today = data.filter(x => moment().diff(x.date, 'days') == 0);
    return (
        <Grid container spacing={3}>
            {/* Chart */}
            <Grid item xs={12} md={8} lg={9}>
                <Paper className={fixedHeightPaper}>
                    <Chart data={today} />
                </Paper>
            </Grid>
            {/* Recent Deposits */}
            <Grid item xs={12} md={4} lg={3}>
                <Paper className={fixedHeightPaper}>
                    <Deposits data={today} />
                </Paper>
            </Grid>
            {/* Recent Orders */}
            <Grid item xs={12}>
                <Paper className={classes.paper}>
                    <Orders data={data.sort((x, y) => x.id > y.id ? 1 : -1).slice(0, 10)} />
                </Paper>
            </Grid>
        </Grid>
    );
}

export default HomeScreen;