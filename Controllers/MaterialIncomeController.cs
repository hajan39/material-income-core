﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MaterialIncomeCore.Models;
using MaterialIncomeCore.Objects;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Options;

namespace MaterialIncomeCore.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MaterialIncomeController : ControllerBase
    {
        private readonly MaterialContext _context;
        private readonly IOptions<SettingsModel> appSettings;
        public MaterialIncomeController(MaterialContext context, IOptions<SettingsModel> app)
        {
            appSettings = app;
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Delivery> GetLimit([FromQuery] DateTime? startDate, [FromQuery] DateTime? endDate, [FromQuery] int limit = 1000, [FromQuery] int step = 0)
        {
            if (startDate == null)
            {
                startDate = DateTime.Now.Date;
            }
            if (endDate == null)
            {
                endDate = DateTime.Now.Date.AddDays(-7);
            }
            return _context.DELIVERY
                .Where(x => x.Deleted == null)
                .Where(x =>   x.DATE <= startDate)
                .Where(x =>   x.DATE >= endDate)
                .OrderByDescending(x => x.Id)
                //.Skip(step * limit)
                //.Take(limit)
                .ToList();
        }
        [HttpGet("delivery/{id}")]
        public Delivery GetDeliveryId(int id)
        {
            return _context.DELIVERY.FirstOrDefault(x => x.Id == id);
        }

        [HttpGet("lic")]
        public IEnumerable<LicencePlates> GetLicences()
        {
            return _context.LICENCE_PLATE_DESC.ToList();
        }

        [HttpGet("Container")]
        public IEnumerable<Container> GetContainer()
        {

            return _context.CONTAINER_DESC.ToList();

        }

        [HttpGet("supplierAddress")]
        public IEnumerable<SupplierAddress> GetSupplierAddress()
        {
            return _context.SUPPLIER_ADDRESS.ToList();
        }
        [HttpGet("supplierPrice")]
        public IEnumerable<SupplierPrice> GetSupplierPrice()
        {

            return _context.SUPPLIER_PRICE.ToList();

        }
        [HttpGet("Note")]
        public IEnumerable<Note> GetNote()
        {

            return _context.NOTE.ToList();

        }
        [HttpGet("DisponentNote")]
        public IEnumerable<DisponentNote> GetDisponentNote()
        {

            return _context.DISPONENT_NOTE.ToList();

        }
        [HttpGet("Carrier")]
        public IEnumerable<Carrier> GetCarrier()
        {

            return _context.CARRIER_DESC.ToList();

        }
        [HttpGet("Supplier")]
        public IEnumerable<Supplier> GetSupplier()
        {

            return _context.SUPPLIER_DESC.ToList();

        }
        [HttpGet("Statuses")]
        public IEnumerable<Statuses> GetStatuses()
        {

            return _context.STATUSES_DESC.ToList();

        }
        [HttpGet("CarSize")]
        public IEnumerable<CarSize> GetCarSize()
        {

            return _context.CAR_SIZE_DESC.ToList();

        }


        [HttpGet("allDesc")]
        public AllDesc GetAllDesc()
        {

            return new AllDesc()
            {
                CarSizes = _context.CAR_SIZE_DESC.ToArray(),
                LicencePlates = _context.LICENCE_PLATE_DESC.ToArray(),
                Carriers = _context.CARRIER_DESC.ToArray(),
                Notes = _context.NOTE.ToArray(),
                Suppliers = _context.SUPPLIER_DESC.ToArray(),
                Statuses = _context.STATUSES_DESC.ToArray(),
                Accepted = _context.DELIVERY.Where(x=> x.Accepted != null).Select(x => x.Accepted).Distinct().OrderBy(x=> x).ToArray()
            };

        }

        //// GET: api/MaterialIncome/5
        //[HttpGet("{id}", Name = "Get")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST: api/MaterialIncome
        [HttpPost]
        public void Post([FromBody] Delivery value)
        {
            _context.DELIVERY.Add(value);
            _context.SaveChanges();
        }

        // PUT: api/MaterialIncome/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Delivery value)
        {
            _context.DELIVERY.Add(value);
            _context.Entry(value).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _context.SaveChanges();
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var delivery = _context.DELIVERY.FirstOrDefault(x => x.Id == id);
            if (delivery != null)
            {
                delivery.Deleted = DateTime.Now;
                _context.DELIVERY.Attach(delivery);
                _context.Entry(delivery).Property(x => x.Deleted).IsModified = true;
                _context.SaveChanges();
            }
        }
    }
}
