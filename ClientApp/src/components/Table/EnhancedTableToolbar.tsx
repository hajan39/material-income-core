import { makeStyles, Theme, createStyles, lighten, Toolbar, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, IconButton, Menu, MenuItem, Tooltip, Typography } from "@material-ui/core";
import clsx from "clsx";
import moment from "moment";
import React from "react";
import { IncomeObject, GetHeaders } from "../../objects/IncomeObject";
import { getData, deleteRecord } from "../../workers/backendWorker";
import XLSX from 'xlsx'
import DeleteIcon from '@material-ui/icons/Delete';
import RefreshIcon from '@material-ui/icons/Refresh';

const useToolbarStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            paddingLeft: theme.spacing(2),
            paddingRight: theme.spacing(1),

            justifyContent: "flex-end"
        },
        highlight:
            theme.palette.type === 'light'
                ? {
                    color: theme.palette.secondary.main,
                    backgroundColor: lighten(theme.palette.secondary.light, 0.85),
                }
                : {
                    color: theme.palette.text.primary,
                    backgroundColor: theme.palette.secondary.dark,
                },
        title: {
            flex: '1 1 100%',
            fontWeight: 900
        },
    }),
);

interface EnhancedTableToolbarProps {
    selected: string[];
    rows: IncomeObject[]
    updateData: (data: IncomeObject[]) => void;
}

const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const [open, setOpen] = React.useState(false);
    const classes = useToolbarStyles();
    const { selected, rows } = props;
    const numSelected = selected.length;
    const exportXlsx = () => {
        let rws: any = rows;
        if (numSelected > 0) {
            rws = rows.filter(x => selected.some(y => y == x.id.toString()))
        }

        rws.unshift(GetHeaders());
        let binaryWS = XLSX.utils.json_to_sheet(rws, { skipHeader: true });

        // Create a new Workbook
        var wb = XLSX.utils.book_new()

        // Name your sheet
        XLSX.utils.book_append_sheet(wb, binaryWS, 'Binary values')
        const fileName = 'prijem_materialu_' + moment().format('YYYY-MM-DD_hh:mm:ss').toString() + ".xlsx";

        // export your excel
        XLSX.writeFile(wb, fileName);
    }

    return (
        <Toolbar
            className={clsx(classes.root, {
                [classes.highlight]: numSelected > 0,
            })}
        >
            {numSelected > 0 && (
                <Typography className={classes.title} color="inherit" variant="subtitle1" component="div">
                    {numSelected} vybrano
                </Typography>
            )}

            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={(event: React.MouseEvent<HTMLButtonElement>) => setAnchorEl(event.currentTarget)}>
                Export
            </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={() => setAnchorEl(null)}
            >
                <MenuItem onClick={() => { setAnchorEl(null); exportXlsx(); }}>Excel</MenuItem>
            </Menu>
            <Tooltip title="Obnovit">
                <IconButton aria-label="update">
                    <RefreshIcon onClick={() => getData().then((data: IncomeObject[]) => props.updateData(data))} />
                </IconButton>
            </Tooltip>

            {numSelected > 0 && (
                <Tooltip title="Delete">
                    <IconButton aria-label="delete">
                        <DeleteIcon color="error" onClick={() => setOpen(true)} />
                    </IconButton>
                </Tooltip>

            )}
            <Dialog
                open={open}
                onClose={() => setOpen(false)}
                aria-labelledby="draggable-dialog-title"
            >
                <DialogTitle id="draggable-dialog-title">
                    Opravdu smazat?
            </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Opravdu chcete odebrat zaznamy? Celkem {numSelected}.
                </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={() => { setOpen(false); }} color="primary">
                        Zrusit
                    </Button>
                    <Button onClick={() => {
                        setOpen(false);
                        selected.forEach(element => {
                            deleteRecord(element);
                        });
                    }} color="primary">
                        Smazat
                </Button>
                </DialogActions>
            </Dialog>
        </Toolbar >
    );
};

export default EnhancedTableToolbar;