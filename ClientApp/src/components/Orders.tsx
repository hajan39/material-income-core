import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from './Title';
import { GetIncomeColumnsMaterial, IncomeObject } from '../objects/IncomeObject';
import { Link } from 'react-router-dom';
import moment from 'moment';

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export interface OrdersProps {
  data: IncomeObject[]
}

export default function Orders(props: OrdersProps) {
  const classes = useStyles();
  const headers = GetIncomeColumnsMaterial();
  return (
    <React.Fragment>
      <Title>Posledni prijezdy</Title>
      <Table size="small">
        <TableHead>
          <TableRow>
            {headers.map((headCell) => (
              <TableCell
                key={headCell.id}
                align={'center'}
                padding={headCell.disablePadding ? 'none' : 'default'}
              >
                {headCell.label}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {props.data.map((row) => (
            <TableRow key={row.id}>
              <TableCell component="th" scope="row">
                {row?.id || ""}
              </TableCell>
              <TableCell align="center">{moment(row?.date).format("DD.MM.YYYY") || ""}</TableCell>
              <TableCell align="center">{moment(row?.arriveTime).format("HH:mm") || ""}</TableCell>
              <TableCell align="center">{moment(row?.departureTime).format("HH:mm") || ""}</TableCell>
              <TableCell align="center">{row?.supplier || ""}</TableCell>
              <TableCell align="center">{row?.carrier || ""}</TableCell>
              <TableCell align="center">{row?.licencePlate || ""}</TableCell>
              <TableCell align="center">{row?.carSize || ""}</TableCell>
              <TableCell align="center">{row?.weight || ""}</TableCell>
              <TableCell align="center">{row?.grn || ""}</TableCell>
              <TableCell align="center">{row?.palletCount || ""}</TableCell>
              <TableCell align="center">{row?.palletDesc || ""}</TableCell>
              <TableCell align="center">{row?.regressionId || ""}</TableCell>
              <TableCell align="center">{row?.importPrice || ""}</TableCell>
              <TableCell align="center">{row?.invoice || ""}</TableCell>
              <TableCell align="center">{row?.deliveryList || ""}</TableCell>
              <TableCell align="center">{row?.cmr || ""}</TableCell>
              <TableCell align="center">{row?.recieptNote || ""}</TableCell>
              <TableCell align="center">{row?.dispatchersNote || ""}</TableCell>
              <TableCell align="center">{row?.accepted || ""}</TableCell>
              <TableCell align="center">{row?.ccs || ""}</TableCell>
              <TableCell align="center">{row?.status || ""}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <div className={classes.seeMore}>
        <Link color="primary" to="/list">
          Zobrazit více
        </Link>
      </div>
    </React.Fragment>
  );
}
