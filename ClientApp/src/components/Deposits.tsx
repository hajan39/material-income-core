import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Title from './Title';
import moment from 'moment';
import { IncomeObject } from '../objects/IncomeObject';

function preventDefault(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) {
  event.preventDefault();
}

const useStyles = makeStyles({
  depositContext: {
    flex: 1,
  },
});


export interface DepositsProps {
  data: IncomeObject[]
}

export default function Deposits(props: DepositsProps) {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Title>Pocet preprav</Title>
      <Typography component="p" variant="h4">
        {props.data.length}
      </Typography>
      <Typography color="textSecondary" className={classes.depositContext}>
        k {moment().format("DD.MM.YYYY hh:mm:ss")}
      </Typography>
      {/* <div>
        <Link color="primary" href="#" onClick={preventDefault}>
          View balance
        </Link>
      </div> */}
    </React.Fragment>
  );
}
