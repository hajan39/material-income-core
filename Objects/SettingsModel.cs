﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MaterialIncomeCore.Objects
{
    public class SettingsModel
    {
        public string[] AllowedOrigins { get; set; }
        public string ConnectionString { get; set; }
    }
}
