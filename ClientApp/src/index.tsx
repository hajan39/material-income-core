import React from 'react';

import 'react-tabulator/lib/styles.css'; // required styles
import 'react-tabulator/lib/css/tabulator_midnight.min.css'; // theme
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import Dashboard from './Dashboard';
import {
  BrowserRouter as Router,
} from "react-router-dom";
import { createMuiTheme, ThemeProvider } from '@material-ui/core';

import blue from '@material-ui/core/colors/blue';


const baseUrl = document.getElementsByTagName('base')[0].getAttribute('href');
const rootElement = document.getElementById('root');

const darkTheme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: blue
  },
});
ReactDOM.render(
  <React.StrictMode>
    <Router>
      <ThemeProvider theme={darkTheme}>
        <Dashboard />
      </ThemeProvider>
    </Router>
  </React.StrictMode>,
  rootElement);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
