import { makeStyles, Theme, createStyles, Paper, Button, Checkbox, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Link as Mlink, Table, TableBody, TableCell, TableContainer, TablePagination, TableRow } from "@material-ui/core";
import moment from "moment";
import React, { FunctionComponent, useState, useEffect } from "react";
import { IncomeObject, IncomeObjectRequest } from "../objects/IncomeObject";
import { deleteRecord } from "../workers/backendWorker";
import EnhancedTableHead from "../components/Table/EnhancedTableHead"
import EnhancedTableToolbar from "../components/Table/EnhancedTableToolbar"
import { Link } from "react-router-dom";


function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

export type Order = 'asc' | 'desc';

function getComparator<Key extends keyof IncomeObjectRequest>(
    order: Order,
    orderBy: Key,
): (a: { [key in Key]: number | string | null }, b: { [key in Key]: number | string | null }) => number {

    return order === 'desc'
        ? (a, b) => descendingComparator(a, b, orderBy)
        : (a, b) => -descendingComparator(a, b, orderBy);

}

function stableSort<T>(array: T[], comparator: (a: T, b: T) => number) {
    const stabilizedThis = array.map((el: T, index: number) => [el, index] as [T, number]);
    stabilizedThis.sort((a, b) => {
        if (a && b) {

            const order = comparator(a[0], b[0]);
            if (order !== 0) return order;
            return a[1] - b[1];
        } else {
            return 1;
        }
    });
    return stabilizedThis.map((el: [T, number]) => el[0]);
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
        },
        paper: {
            width: '100%',
            marginBottom: theme.spacing(2),
        },
        table: {
            minWidth: 750,
        },
        visuallyHidden: {
            border: 0,
            clip: 'rect(0 0 0 0)',
            height: 1,
            margin: -1,
            overflow: 'hidden',
            padding: 0,
            position: 'absolute',
            top: 20,
            width: 1,
        },
    }),
);

export interface ListScreenProps {
    rowsInput: IncomeObject[];
    updateData: (data: IncomeObject[]) => void;
}

const ListScreen: FunctionComponent<ListScreenProps> = ({ rowsInput, updateData }) => {
    const [rows, setRows] = useState(rowsInput);
    const [filters, setFilters] = useState<{ [key: string]: string }>({});
    const classes = useStyles();
    const [order, setOrder] = React.useState<Order>('asc');
    const [orderBy, setOrderBy] = React.useState<keyof IncomeObject>('id');
    const [selected, setSelected] = React.useState<string[]>([]);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [open, setOpen] = React.useState(false);

    useEffect(() => {
        setRows(rowsInput)
    }, [rowsInput])

    const deleteRow = () => {
        setOpen(false);
        if (toBeDeleted)
            deleteRecord(toBeDeleted).then((data: IncomeObject[]) => {
                updateData(data);
            })
    }

    const handleRequestSort = (event: React.MouseEvent<unknown>, property: keyof IncomeObject) => {
        const isAsc = orderBy === property && order === 'asc';
        setOrder(isAsc ? 'desc' : 'asc');
        setOrderBy(property);
    };

    const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.checked) {
            const newSelecteds = rows.map((n) => n.id.toString());
            setSelected(newSelecteds);
            return;
        }
        setSelected([]);
    };

    const handleClick = (event: React.MouseEvent<unknown>, id: number | string) => {
        const name = id.toString();
        const selectedIndex = selected.indexOf(name);

        if (selectedIndex === -1) {
            setSelected([...selected, name]);
        } else if (selectedIndex === 0) {
            setSelected(selected.slice(1));
        } else if (selectedIndex > 0) {
            setSelected([...selected.slice(0, selectedIndex), ...selected.slice(selectedIndex + 1)])

        }
    };

    const handleFilterChange = (id: string, value: string) => {
        var newfil = filters;
        var newRows = rowsInput;
        if (!value) {
            delete newfil[id];
        } else {
            newfil[id] = value;
        }
        setFilters(newfil);
        Object.keys(newfil).forEach(key => {
            newRows = newRows.filter(x => {
                if (x[key] !== null && newfil[key] !== null)
                    return x[key].toString().includes(newfil[key]);
                return false;
            })
        });
        setRows(newRows)
    }

    const isSelected = (name: string) => selected.indexOf(name) !== -1;

    const [toBeDeleted, setTtoBeDeleted] = React.useState<number | string>();

    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <EnhancedTableToolbar selected={selected} rows={rows} updateData={updateData} />
                <TableContainer>
                    <Table stickyHeader
                        className={classes.table}
                        aria-labelledby="tableTitle"
                        size={'small'}
                        aria-label="enhanced table"
                    >
                        <EnhancedTableHead
                            numSelected={selected.length}
                            order={order}
                            orderBy={orderBy}
                            onSelectAllClick={handleSelectAllClick}
                            onRequestSort={handleRequestSort}
                            rowCount={rows.length}
                            filterChanged={handleFilterChange}
                        />
                        <TableBody>
                            {stableSort(rows, getComparator(order, orderBy))
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map((row, index) => {
                                    const isItemSelected = isSelected(row.id.toString());
                                    const labelId = `enhanced-table-checkbox-${index}`;
                                    console.log("row", row);

                                    return (
                                        <TableRow
                                            hover
                                            onClick={(event) => handleClick(event, row.id)}
                                            role="checkbox"
                                            aria-checked={isItemSelected}
                                            tabIndex={-1}
                                            key={row.id}
                                            selected={isItemSelected}
                                        >
                                            <TableCell padding="checkbox">
                                                <Checkbox
                                                    checked={isItemSelected}
                                                    inputProps={{ 'aria-labelledby': labelId }}
                                                />
                                            </TableCell>
                                            <TableCell component="th" scope="row">
                                                {row?.id || ""}
                                            </TableCell>
                                            <TableCell align="center">{moment(row?.date).format("DD.MM.YYYY") || ""}</TableCell>
                                            <TableCell align="center">{moment(row?.arriveTime).format("HH:mm") || ""}</TableCell>
                                            <TableCell align="center">{moment(row?.departureTime).format("HH:mm") || ""}</TableCell>
                                            <TableCell align="center">{row?.supplier || ""}</TableCell>
                                            <TableCell align="center">{row?.carrier || ""}</TableCell>
                                            <TableCell align="center">{row?.licencePlate || ""}</TableCell>
                                            <TableCell align="center">{row?.carSize || ""}</TableCell>
                                            <TableCell align="center">{row?.weight || ""}</TableCell>
                                            <TableCell align="center">{row?.grn || ""}</TableCell>
                                            <TableCell align="center">{row?.palletCount || ""}</TableCell>
                                            <TableCell align="center">{row?.palletDesc || ""}</TableCell>
                                            <TableCell align="center">{row?.regressionId || ""}</TableCell>
                                            <TableCell align="center">{row?.importPrice || ""}</TableCell>
                                            <TableCell align="center">{row?.invoice || ""}</TableCell>
                                            <TableCell align="center">{row?.deliveryList || ""}</TableCell>
                                            <TableCell align="center">{row?.cmr || ""}</TableCell>
                                            <TableCell align="center">{row?.recieptNote || ""}</TableCell>
                                            <TableCell align="center">{row?.dispatchersNote || ""}</TableCell>
                                            <TableCell align="center">{row?.accepted || ""}</TableCell>
                                            <TableCell align="center">{row?.ccs || ""}</TableCell>
                                            <TableCell align="center">{row?.status || ""}</TableCell>
                                            <TableCell align="center"><Link to={"insert/" + row.id}>Upravit</Link></TableCell>
                                            <TableCell align="center"><Mlink style={{ color: "red" }} onClick={() => {
                                                setTtoBeDeleted(row.id);
                                                setOpen(true)
                                            }}>Smazat</Mlink></TableCell>
                                        </TableRow>
                                    );
                                })}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 50, 100]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={(event: React.MouseEvent<HTMLButtonElement, MouseEvent> | null, newPage: number) => setPage(newPage)}
                    onChangeRowsPerPage={(event: React.ChangeEvent<HTMLInputElement>) => {
                        setRowsPerPage(parseInt(event.target.value, 10));
                        setPage(0);
                    }}
                />
            </Paper>
            <Dialog
                open={open}
                onClose={() => setOpen(false)}
                aria-labelledby="draggable-dialog-title"
            >
                <DialogTitle id="draggable-dialog-title">
                    Smazat?
        </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Opravdu chcete odebrat zaznam cislo {toBeDeleted}?
          </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={() => {
                        setOpen(false);
                        setTtoBeDeleted(undefined);
                    }} color="primary">
                        Zrusit
                    </Button>
                    <Button onClick={() => {
                        deleteRow()
                    }} color="primary">
                        Smazat
                    </Button>
                </DialogActions>
            </Dialog>
        </div >
    );
}
export default ListScreen;